from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from UmlViewer.serializers import ProjectSerializer, ProjectUserSerializer, UmlObjectSerializer, UmlRelationSerializer, \
    ObjectSerializer
from UmlViewer.models import Project, ProjectUser, UmlObject, UmlRelation
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from django.db.models import Sum

class TreeViewSet(ViewSet):
    permission_classes = [IsAuthenticated]
    def retrieve(self, request, pk=None):
        queryset = UmlObject.objects.filter(project_id=pk, parent_object__isnull=True)
        serializer = ObjectSerializer(queryset, many=True)
        return Response(serializer.data, status=200)

class ProjectViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = Project.objects.order_by('pk')
        serializer = ProjectSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            mainframe = {
                'name': serializer.data['name'],
                'project_id': serializer.data['id'],
                'description': 'To jest ramka główna projektu.',
                'object_type': 'f'
            }
            frame_serializer = UmlObjectSerializer(data=mainframe)
            if frame_serializer.is_valid():
                frame_serializer.save()
                return Response(serializer.data, status=201)
            else:
                Project.objects.filter(pk=serializer.data['id']).delete()
                return Response(serializer.errors, status=400)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Project.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = ProjectSerializer(item)
        return Response(serializer.data)

    def update(self, request, pk=None):
        try:
            item = Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            return Response(status=404)
        res = request.data
        res['mod_user'] = request.user.id
        serializer = ProjectSerializer(item, data=res)
        if serializer.is_valid():
            serializer.save()
            mainframe = UmlObject.objects.filter(project_id=serializer.data['id'], parent_object__isnull=True).first()
            mainframe.name = serializer.data['name']
            frame = {
                'id': mainframe.id,
                'name': serializer.data['name'],
                'project_id': serializer.data['id']
            }
            frame_serializer = UmlObjectSerializer(mainframe, data=frame)
            if frame_serializer.is_valid():
                frame_serializer.save()
                return Response(serializer.data, status=200)
            else:
                return Response(frame_serializer.errors, status=400)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class ProjectUserViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = ProjectUser.objects.order_by('pk')
        serializer = ProjectUserSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        res = request.data
        res['mod_user'] = request.user.id
        serializer = ProjectUserSerializer(data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = ProjectUser.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = ProjectUserSerializer(item)
        return Response(serializer.data)

    def update(self, request, pk=None):
        try:
            item = ProjectUser.objects.get(pk=pk)
        except ProjectUser.DoesNotExist:
            return Response(status=404)
        res = request.data
        res['mod_user'] = request.user.id
        serializer = ProjectUserSerializer(item, data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = ProjectUser.objects.get(pk=pk)
        except ProjectUser.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class UmlObjectViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = UmlObject.objects.order_by('pk')
        serializer = UmlObjectSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        res = request.data
        res['mod_user'] = request.user.id
        serializer = UmlObjectSerializer(data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = UmlObject.objects.all()
        item = get_object_or_404(queryset, pk=pk)
        serializer = UmlObjectSerializer(item)
        return Response(serializer.data)

    def update(self, request, pk=None):
        try:
            item = UmlObject.objects.get(pk=pk)
        except UmlObject.DoesNotExist:
            return Response(status=404)
        res = request.data
        res['mod_user'] = request.user.id
        serializer = UmlObjectSerializer(item, data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = UmlObject.objects.get(pk=pk)
        except UmlObject.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


class UmlRelationViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        queryset = UmlRelation.objects.order_by('pk')
        serializer = UmlRelationSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        res = request.data
        res['mod_user'] = request.user.id
        serializer = UmlRelationSerializer(data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = UmlRelation.objects.filter(Q(rel_beg=pk) | Q(rel_end=pk))
        serializer = UmlRelationSerializer(queryset, many=True)
        return Response(serializer.data)

    def update(self, request, pk=None):
        try:
            item = UmlRelation.objects.get(pk=pk)
        except UmlRelation.DoesNotExist:
            return Response(status=404)
        res = request.data
        res['mod_user'] = request.user.id
        serializer = UmlRelationSerializer(item, data=res)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

    def destroy(self, request, pk=None):
        try:
            item = UmlRelation.objects.get(pk=pk)
        except UmlRelation.DoesNotExist:
            return Response(status=404)
        item.delete()
        return Response(status=204)


def get_code(pk):
    code = ''
    for frame in UmlObject.objects.filter(id=pk).all():
        if frame.object_type == 'f':
            code += 'rectangle "' + frame.name + '" {\n'
            for subObject in UmlObject.objects.filter(parent_object=frame.id).all():
                if subObject.object_type == 'a':
                    code += 'actor ' + subObject.object_type + str(subObject.id) + ' as "' + subObject.name + '"\n'
                if subObject.object_type == 'u':
                    code += 'usecase ' + subObject.object_type + str(subObject.id) + ' as "' + subObject.name
                    if subObject.description is not None:
                        code += '\n==\n ' + subObject.description
                    code += '"\n'
                if subObject.object_type == 'f':
                    code += get_code(subObject.id)
            code += '}\n'
    return code


class PlantUmlViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, pk=None):
        object = UmlObject.objects.filter(project_id=pk, parent_object__isnull=True).first()
        code = '@startuml \n' + get_code(object.id)
        for rel in UmlRelation.objects.all():
            reltyp = ' -- '
            if rel.relation_type == 'so':
                reltyp = ' -- '
            if rel.relation_type == 'sa':
                reltyp = ' -> '
            if rel.relation_type == 'da':
                reltyp = ' ..> '
            stereotype = ''
            if rel.stereotype == 'in':
                stereotype = ' : include '
            if rel.stereotype == 'ex':
                stereotype = ' : exclude '
            if rel.stereotype == 'ih':
                stereotype = ' : inheritance '
            code += getattr(UmlObject.objects.filter(id=rel.rel_beg_id).first(), 'object_type') + str(rel.rel_beg_id) + \
                    reltyp + getattr(UmlObject.objects.filter(id=rel.rel_end_id).first(), 'object_type') + \
                    str(rel.rel_end_id) + stereotype + '\n'

        code += '@enduml'
        return Response(code, status=200)


class EstimateCostViewSet(ViewSet):
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, pk=None):
        query = Project.objects.filter(id=pk).first()

        ecf = 1.4 + (-0.03 * (getattr(query, 'ECF1') * 1.5 +
                              getattr(query, 'ECF2') * 0.5 +
                              getattr(query, 'ECF3') * 1.0 +
                              getattr(query, 'ECF4') * 0.5 +
                              getattr(query, 'ECF5') * 1.0 +
                              getattr(query, 'ECF6') * 2.0 +
                              getattr(query, 'ECF7') * -1.0 +
                              getattr(query, 'ECF8') * -1.0))
        tcf = 0.6 + (-0.01 * (getattr(query, 'TCF1') * 2.0 +
                              getattr(query, 'TCF2') * 1.0 +
                              getattr(query, 'TCF3') * 1.0 +
                              getattr(query, 'TCF4') * 1.0 +
                              getattr(query, 'TCF5') * 1.0 +
                              getattr(query, 'TCF6') * 0.5 +
                              getattr(query, 'TCF7') * 0.5 +
                              getattr(query, 'TCF8') * 2.0 +
                              getattr(query, 'TCF9') * 1.0 +
                              getattr(query, 'TCF10') * 1.0 +
                              getattr(query, 'TCF11') * 1.0 +
                              getattr(query, 'TCF12') * 1.0 +
                              getattr(query, 'TCF13') * 1.0))

        query = UmlObject.objects.filter(project_id=pk).all().aggregate(Sum('UAW'))
        uaw = query['UAW__sum']
        query = UmlObject.objects.filter(project_id=pk).all().aggregate(Sum('UUCW'))
        uucw = query['UUCW__sum']
        if uaw is None:
            uaw = 0
        if uucw is None:
            uucw = 0
        uucp = uaw + uucw
        ucp = uucp * tcf * ecf * 20
        return Response(round(ucp, 3), status=200)

