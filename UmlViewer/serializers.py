from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from UmlViewer.models import Project, ProjectUser, UmlObject, UmlRelation


class ProjectSerializer(ModelSerializer):

    class Meta:
        model = Project
        fields = '__all__'


class ProjectUserSerializer(ModelSerializer):

    class Meta:
        model = ProjectUser
        fields = '__all__'


class UmlObjectSerializer(ModelSerializer):

    class Meta:
        model = UmlObject
        fields = '__all__'


class UmlRelationSerializer(ModelSerializer):

    class Meta:
        model = UmlRelation
        fields = '__all__'


class ObjectSerializer(ModelSerializer):
    child_objects = SerializerMethodField()

    class Meta:
        model = UmlObject
        fields = [
            'id', 'object_type', 'name', 'child_objects', 'description', 'parent_object'
        ]

    def get_child_objects(self, obj):
        return ObjectSerializer(obj.umlobject_set.all(), many=True).data

