from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(UmlObject)
admin.site.register(Project)
admin.site.register(ProjectUser)
admin.site.register(UmlRelation)