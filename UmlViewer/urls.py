from rest_framework.routers import SimpleRouter
from UmlViewer import views


router = SimpleRouter()

router.register(r'project', views.ProjectViewSet, 'Project')
router.register(r'plantuml', views.PlantUmlViewSet, 'PlantUml')
router.register(r'treeview', views.TreeViewSet, 'TreeView')
router.register(r'projectuser', views.ProjectUserViewSet, 'ProjectUser')
router.register(r'umlobject', views.UmlObjectViewSet, 'UmlObject')
router.register(r'umlrelation', views.UmlRelationViewSet, 'UmlRelation')
router.register(r'estimatecost', views.EstimateCostViewSet, 'EstimateCost')

urlpatterns = router.urls
