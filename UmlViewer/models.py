from django.db import models
from django.conf import settings

class AbstractEveryModel(models.Model):
    add_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        blank=True,
        null=True,
        related_name='%(class)s_add_user'
    )
    add_date = models.DateTimeField(auto_now_add=True)
    mod_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        blank=True,
        null=True,
        related_name='%(class)s_mod_user'
    )
    mod_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True
        app_label = 'UmlViewer'


class Project(AbstractEveryModel):
    complexity = [
        (0, '0'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    ]
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    sh_name = models.CharField(max_length=10, blank=True, null=True)
    ECF1 = models.IntegerField(choices=complexity, default='0')
    ECF2 = models.IntegerField(choices=complexity, default='0')
    ECF3 = models.IntegerField(choices=complexity, default='0')
    ECF4 = models.IntegerField(choices=complexity, default='0')
    ECF5 = models.IntegerField(choices=complexity, default='0')
    ECF6 = models.IntegerField(choices=complexity, default='0')
    ECF7 = models.IntegerField(choices=complexity, default='0')
    ECF8 = models.IntegerField(choices=complexity, default='0')
    TCF1 = models.IntegerField(choices=complexity, default='0')
    TCF2 = models.IntegerField(choices=complexity, default='0')
    TCF3 = models.IntegerField(choices=complexity, default='0')
    TCF4 = models.IntegerField(choices=complexity, default='0')
    TCF5 = models.IntegerField(choices=complexity, default='0')
    TCF6 = models.IntegerField(choices=complexity, default='0')
    TCF7 = models.IntegerField(choices=complexity, default='0')
    TCF8 = models.IntegerField(choices=complexity, default='0')
    TCF9 = models.IntegerField(choices=complexity, default='0')
    TCF10 = models.IntegerField(choices=complexity, default='0')
    TCF11 = models.IntegerField(choices=complexity, default='0')
    TCF12 = models.IntegerField(choices=complexity, default='0')
    TCF13 = models.IntegerField(choices=complexity, default='0')

    class Meta:
        abstract = False
        app_label = 'UmlViewer'


class ProjectUser(AbstractEveryModel):
    user_id = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.PROTECT,
        related_name='%(class)s_user_id',
    )
    project_id = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='%(class)s_project_id',
    )

    class Meta:
        abstract = False
        app_label = 'UmlViewer'


class UmlObject(AbstractEveryModel):
    complexity_uaw = [
        (1, 'Prosty'),
        (2, 'Średnio złożony'),
        (3, 'Złożony'),
    ]
    complexity_uucw = [
        (5, 'Przypadki proste – jeśli mają 3 lub mniej transakcji'),
        (10, 'Średnie – liczba transakcji od 4 do 7'),
        (15, 'Złożone – powyżej 7 transakcji'),
    ]
    difficulty_dict = [
        ("si", "Prosty"),
        ("no", "Normalny"),
        ("di", "Trudny"),
    ]
    priority_dict = [
        ("low", "Niski"),
        ("med", "Normalny"),
        ("hig", "Wysoki"),
        ("imm", "Natychmiastowy"),
    ]
    object_type_dict = [
        ("u", "Przypadek użycia"),
        ("a", "Aktor"),
        ("f", "Ramka"),
    ]
    object_type = models.CharField(max_length=1, choices=object_type_dict, default="u")
    project_id = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='%(class)s_project_id',
    )
    parent_object = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=50, blank=True, null=True)
    priority = models.CharField(max_length=3, blank=True, null=True, choices=priority_dict, default="med")
    difficulty = models.CharField(max_length=2, blank=True, null=True, choices=difficulty_dict, default="no")
#    Leveldescription = models.CharField(max_length=10, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
#    CostTime = models.IntegerField(blank=True, null=True)
    uc_purpose = models.CharField(max_length=500, blank=True, null=True)
    sys_purpose = models.CharField(max_length=500, blank=True, null=True)
    fun_requirement = models.CharField(max_length=500, blank=True, null=True)
    non_fun_requirement = models.CharField(max_length=500, blank=True, null=True)
    quality_condition = models.CharField(max_length=500, blank=True, null=True)
    precondition = models.CharField(max_length=500, blank=True, null=True)
    suc_condition = models.CharField(max_length=500, blank=True, null=True)
    fail_condition = models.CharField(max_length=500, blank=True, null=True)
    business_req = models.CharField(max_length=500, blank=True, null=True)
    error_risk = models.CharField(max_length=500, blank=True, null=True)
    main_actor = models.CharField(max_length=500, blank=True, null=True)
    sec_actor = models.CharField(max_length=500, blank=True, null=True)
    trigger = models.CharField(max_length=500, blank=True, null=True)
    main_scenario = models.CharField(max_length=500, blank=True, null=True)
    alter_scenario = models.CharField(max_length=500, blank=True, null=True)
    exc_scenario = models.CharField(max_length=500, blank=True, null=True)
    UAW = models.IntegerField(choices=complexity_uaw, blank=True, null=True)
    UUCW = models.IntegerField(choices=complexity_uucw, blank=True, null=True)

    class Meta:
        abstract = False
        app_label = 'UmlViewer'


class UmlRelation(AbstractEveryModel):
    stereotype_dict = [
        ("in", "include"),
        ("ex", "extend"),
        ("ih", "inheritance"),
    ]
    relation_type_dict = [
        ("so", "Linia ciągła"),
        ("sa", "Strzałka ciągła"),
        ("da", "Strzałka przerywana"),
    ]
    rel_beg = models.ForeignKey(
        UmlObject,
        on_delete=models.CASCADE,
        related_name='%(class)s_rel_beg',
    )
    rel_end = models.ForeignKey(
        UmlObject,
        on_delete=models.CASCADE,
        related_name='%(class)s_rel_end',
    )
    relation_type = models.CharField(max_length=2, choices=relation_type_dict)
    name = models.CharField(blank=True, max_length=50)
    stereotype = models.CharField(max_length=2, blank=True, null=True, choices=stereotype_dict)

    class Meta:
        abstract = False
        app_label = 'UmlViewer'
