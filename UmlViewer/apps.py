from django.apps import AppConfig


class UmlviewerConfig(AppConfig):
    name = 'UmlViewer'
